<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
   
    public function __construct() {
	parent::__construct();
		$this->load->library('datatables');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('User_model','user_model');
		$this->load->model('Vendor_model', 'vendor_model');
		$this->load->helper('uuid_helper');
 	}

	public function index()
	{
		$vendors = $this->vendor_model->allVendor();

		$data['admin']=$this->session->userdata;
		$data['title']    = 'User';
		$data['contents'] = 'user';
		$data['vendors'] = $vendors;

		$this->load->view('layout/app', $data);
	}

	public function create()
	{
		$nip  = $this->input->post('nip');
		$namaUser  = $this->input->post('nama_user');
		$tipeUSerGroup  = $this->input->post('tipe_user_group');
		$levelUser  = $this->input->post('level_user');
		$kodeGroup  = $this->input->post('kode_group');

		$dataUser = array(
			'uuid_user'			=> gen_uuid(),
			'nip' 				=> $nip,
			'nama_user' 		=> $namaUser,
			'tipe_user_group' 	=> $tipeUSerGroup,
			'level_user' 		=> $levelUser,
			'kode_group' 		=> $kodeGroup,
			'password' 			=> $this->hash_password('123456'),
			'status' 			=> '1'
		);

		$check_exist_nip = $this->user_model->getWhere(array('nip' => $nip));

		if (empty($check_exist_nip)) {
			$create_user = $this->user_model->insert($dataUser);
			echo json_encode([
				'status' 	=> true,
				'message' 	=> 'User added successfully.'
			]);
		} else {
			echo json_encode([
				'status' 	=> false,
				'message' => 'NIP Sudah Terdaftar.'
			]);
		}
	}

	public function getUserJson()
	{
		if (defined('BASEPATH') && !$this->input->is_ajax_request()) {
			exit('404');
		}

		header('Content-Type: application/json');
		echo $this->user_model->getAllUser();
	}

	public function edit()
	{
		$uuid       = $this->input->get('uuid');
		$data      	= $this->user_model->getDataEdit($uuid);
		$users  	= $this->user_model->getAllDataUser();

		array_unshift($users,  [
			'id' => 0,
			'name' => 'No Parents',
			'display_name' => 'No Parents',
			'description' => 'No Parents',
			'parent_id' => ''
		]);

		$result = array(
			'data' 			=> $data,
			'users' 		=> $users
		);

		$this->load->view('user/modal_edit', $result);
	}

	public function update()
	{
		$data = array(
			'nip'   		=> $this->input->post('nip'),
			'nama_user'  	=> $this->input->post('nama_user'),
			'status'   		=> $this->input->post('status'),
		);

		$checkUnique = $this->user_model->checkIsUnique($data['nip'], $this->input->post('uuid_user'));

		if (!$checkUnique) {
			$update = $this->user_model->update_user($data, $this->input->post('uuid_user'));
			if ($update) {
				echo json_encode([
					'status' 	=> true,
					'message' 	=> 'User updated successfully.'
				]);
			} else {
				echo json_encode([
					'status' 	=> true,
					'message' 	=> 'User updated failed.'
				]);
			}
		} else {
			echo json_encode([
				'status' 	=> false,
				'message' => 'NIP Sudah Terdaftar.'
			]);
		}
	}

	private function hash_password($password)
	{
		return password_hash($password, PASSWORD_BCRYPT);
	}
}
