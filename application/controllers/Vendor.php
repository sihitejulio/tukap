<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {
   
    public function __construct() {
	parent::__construct();
		$this->load->library('datatables');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Vendor_model','vendor_model');
		$this->load->helper('uuid_helper');
 	}

	public function index()
	{
		$data['admin']=$this->session->userdata;

		$data['title']    = 'Vendor';
		$data['contents'] = 'vendor';
		$this->load->view('layout/app', $data);
	}
	
	public function create() {
		$kodeVendor  = $this->input->post('kode_vendor');
		$namaVendor  = $this->input->post('nama_vendor');

		$dataVendor = array(
			'uuid_vendor'	=> gen_uuid(),
			'kode_vendor' 	=> $kodeVendor,
			'nama_vendor' 	=> $namaVendor,
			'status' 		=> '1',
			'tgl_create'	=> date('Y-m-d H:i:s'), 	
			'tgl_update'	=> date('Y-m-d H:i:s'), 	
		);

		$check_exist_kode_vendor = $this->vendor_model->getWhere(array('kode_vendor' => $kodeVendor));

		if (empty($check_exist_kode_vendor)) {
			$create_vendor = $this->vendor_model->insert($dataVendor);
			echo json_encode([
				'status' 	=> true,
				'message' 	=> 'Vendor added successfully.'
			]);
		} else {
			echo json_encode([
				'status' 	=> false,
				'message' => 'Kode Vendor Sudah Terdaftar.'
			]);
		}
	}

	public function getVendorJson()
	{
		if (defined('BASEPATH') && !$this->input->is_ajax_request()) {
			exit('404');
		}

		header('Content-Type: application/json');
		echo $this->vendor_model->getAllVendor();
	}

	public function edit()
	{
		$uuid       = $this->input->get('uuid');
		$data      	= $this->vendor_model->getDataEdit($uuid);
		$vendors  	= $this->vendor_model->getAllDataVendor();

		array_unshift($vendors,  [
			'id' => 0,
			'name' => 'No Parents',
			'display_name' => 'No Parents',
			'description' => 'No Parents',
			'parent_id' => ''
		]);

		$result = array(
			'data' 			=> $data,
			'vendors' 		=> $vendors
		);

		$this->load->view('vendor/modal_edit', $result);
	}

	public function update()
	{
		$data = array(
			'kode_vendor'   => $this->input->post('kode_vendor'),
			'nama_vendor'  	=> $this->input->post('nama_vendor'),
			'status'   		=> $this->input->post('status'),
		);

		$checkUnique = $this->vendor_model->checkIsUnique($data['kode_vendor'], $this->input->post('uuid_vendor'));

		if (!$checkUnique) {
			$update = $this->vendor_model->update_vendor($data, $this->input->post('uuid_vendor'));
			if ($update) {
				echo json_encode([
					'status' 	=> true,
					'message' 	=> 'Vendor updated successfully.'
				]);
			} else {
				echo json_encode([
					'status' 	=> true,
					'message' 	=> 'Vendor updated failed.'
				]);
			}
		} else {
			echo json_encode([
				'status' 	=> false,
				'message' => 'Kode Vendor Sudah Terdaftar.'
			]);
		}

	}
}
