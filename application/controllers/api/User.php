<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class User extends CI_Controller {

    use REST_Controller { REST_Controller::__construct as private __resTraitConstruct; }
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

    }

    public function users_get()
    {
        
    }

    public function users_post()
    {
        
    }

    public function users_put()
    {
        
    }

    public function users_delete()
    {
       
    }

}
