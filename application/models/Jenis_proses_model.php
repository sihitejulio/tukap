<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * undocumented class
 */
class Jenis_proses_model extends CI_Model
{
    public function insert($data){
        $INSERT = $this->db->insert('jenis_proses', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function edit($id,$data){
        $this->db->where('jenis_proses', $id);
        $this->db->update('denom', $data);
        return true;
    }


    public function allJenisProses()
    {
        $this->db->select('*');
        $this->db->from('jenis_proses');
        return $this->db->get()->result();
    }

    public function getWhere($array){
        $this->db->select('*');
        $this->db->from('jenis_proses');
        $this->db->where($array);
        return $this->db->get()->row_array();
    }

    
    public function getWhereId($id){
        $this->db->select('*');
        $this->db->from('jenis_proses');
        $this->db->where('uuid_menu',$id);
        return $this->db->get()->row_array();
    }    
}
/* End of file filename.php */
?>