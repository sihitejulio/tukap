<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * undocumented class
 */
class Cabang_model extends CI_Model
{
    public function insert($data){
        $INSERT = $this->db->insert('cabang', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function edit($id,$data){
        $this->db->where('id_cabang', $id);
        $this->db->update('cabang', $data);
        return true;
    }


    public function allCabang()
    {
        $this->db->select('*');
        $this->db->from('cabang');
        return $this->db->get()->result();
    }

    public function getWhere($array){
        $this->db->select('*');
        $this->db->from('cabang');
        $this->db->where($array);
        return $this->db->get()->row_array();
    }
    
    public function getWhereId($id){
        $this->db->select('nama_cabang');
        $this->db->from('cabang');
        $this->db->where('id_cabang',$id);
        return $this->db->get()->row_array();
    }

    
}
/* End of file filename.php */

?>
