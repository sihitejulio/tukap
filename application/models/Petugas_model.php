<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * undocumented class
 */
class User_model extends CI_Model
{
    public function insert($data){
        $INSERT = $this->db->insert('user', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function edit($id,$data){
        $this->db->where('uuid_user', $id);
        $this->db->update('user', $data);
        return true;
    }


    public function allPetugas()
    {
        $this->db->select('*');
        $this->db->from('user');
        return $this->db->get()->result();
    }

    public function getWhere($array){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($array);
        return $this->db->get()->row_array();
    }

    
    public function getWhereId($id){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('uuid_user',$id);
        return $this->db->get()->row_array();
    }
}
/* End of file filename.php */

?>