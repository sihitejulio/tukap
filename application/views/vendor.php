<div class="row">
	<div class="col-md-4 mb-4 mt-4">
		<form action="#" method="post" id="create_vendor" enctype="multipart/form-data" class="form-horizontal">
			<div class="card">
				<div class="card-header">
					<strong>Form Pendaftaran Vendor</strong>
				</div>
				<div class="card-body">
					<div class="form-group row">
						<label class="col-md-4 col-form-label" for="text-input">Kode Vendor</label>
						<div class="col-md-8">
							<input type="text" id="kode_vendor" name="kode_vendor" class="form-control" placeholder="Masukkan Kode Vendor">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-4 col-form-label" for="text-input">Nama Vendor</label>
						<div class="col-md-8">
							<input type="text" id="nama_vendor" name="nama_vendor" class="form-control" placeholder="Masukkan Nama Vendor">
						</div>
					</div>
				</div>
				<div class="card-footer">
					<button type="submit" id="vendor_submit" class="btn btn-sm btn-primary" onclick="submitFormVendor(event)"><i class="fa fa-dot-circle-o"></i> Submit</button>
					<button type="reset" id="vendor_reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-8 mb-4 mt-4">
		<div class="card">
			<div class="card-header">
				<strong>Daftar Vendor</strong>
			</div>
			<div class="card-body table-responsive">
				<table id="table_vendor" class="table-striped table-sm table table-bordered table-condensed table-hover " cellspacing="0" width="tabelListDbs%">
					<thead style="background-color:#bfe7bf">
						<tr>
							<th>Kode Vendor</th>
							<th>Nama Vendor</th>
							<th>Status</th>
							<th>Opsi</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="card-footer">
			</div>
		</div>
	</div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" id="bg-modal">
			<!-- Modal body -->
			<div class="modal-body">
				<div id="result_data"></div>
			</div>
			<!-- Modal footer -->
		</div>
	</div>
</div>

<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>

<script>
	function submitFormVendor(event) {
		event.preventDefault();
		var kode_vendor = document.getElementById("kode_vendor").value;
		var nama_vendor = document.getElementById("nama_vendor").value;

		var formData = new FormData();

		formData.append('kode_vendor', kode_vendor);
		formData.append('nama_vendor', nama_vendor);

		$.confirm({
			title: 'Form Vendor',
			content: 'Submit Vendor ?',
			buttons: {
				confirm: function() {
					$.ajax({
						url: "vendors/create",
						type: 'POST',
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: "json",
						success: function(data) {
							if (data.status) {
								$.alert(data.message);
								window.location.reload();
							} else {
								$.alert(data.message);
							}
						},
						error: function(error) {
							$.alert(error);
						}
					});
				},
				cancel: function() {}
			}
		});
	}
	
	$(document).ready(function() {
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		var tableVendor = $("#table_vendor").DataTable({
			"dom": 'Zlfrtip',
			initComplete: function() {
				var api = this.api();
				$('#table_vendor_filter input')
					.off('.DT')
					.on('input.DT', function() {
						api.search(this.value).draw();
					});
			},
			oLanguage: {
				"sUrl": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Indonesian.json",
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {
				"url": "vendors/getVendorJson",
				"type": "POST"
			},
			columns: [{
					"data": "kode_vendor"
				},
				{
					"data": "nama_vendor"
				},
				{
					"data": "status",
					"render": function(data, type, row, meta) {
						if (data == '1') {
							return 'Aktif';
						} else {
							return 'Tidak Aktif';
						}
					}
				},
				{
					"data": "view"
				}
			],
			order: [
				[0, 'asc']
			],
			rowCallback: function(row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				$('td:eq(0)', row).html();
			}
		});

		$('#table_vendor').on('click', '.editVendor', function() {
			var uuid = $(this).data('uuid_vendor');
			console.log(uuid);
			$('#myModal').modal('show')
			$.ajax({
				type: 'get',
				url: 'vendors/edit?uuid=' + uuid,
				success: function(result) {
					$('#result_data').html(result);
				}
			});
		});
	});
</script>
