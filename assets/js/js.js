$(document).ready(function() {  
    // LOgin
    $(".btn-login").click(function(e){
        // alert("a");
		e.preventDefault();
		var username = $("input[name='username']").val();
		var password = $("input[name='password']").val();
		var tipe     = $('select[name=tipe]').val();


		
		var formData = new FormData();

		formData.append('username', username);
		formData.append('passuser', password);
		// formData.append('tipe', tipe);


		$.ajax({
			url: "login/login_user",
			type:'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				var args = $.parseJSON(data);
				if (args['status_valid'] == 'false') {
					$(".id-help").remove();
					$('#usernameHelp').after(args['err_username']);
					$('#passwordHelp').after(args['err_password']);
					$('#tipeHelp').after(args['err_type']);
				}else{
					if(args['login'] == 'false'){
						$(".print-error-msg").css('display','block');
					    $(".print-error-msg").html(args['login_info']);
					}else{
						$(".print-error-msg").css('display','none');
					    window.location.href=args['url'] ;
					}
				}
				return false;
			}
		});
    }); 
    // $(".btn-login").click(function(e){
	// 	e.preventDefault();
	// 	var username = $("input[name='username']").val();
	// 	var password = $("input[name='password']").val();
	// 	var tipe     = $('select[name=tipe]').val();


		
	// 	var formData = new FormData();

	// 	formData.append('username', username);
	// 	formData.append('passuser', password);
	// 	formData.append('tipe', tipe);


	// 	$.ajax({
	// 		url: "user/login",
	// 		type:'POST',
	// 		data: formData,
	// 		cache: false,
	// 		contentType: false,
	// 		processData: false,
	// 		success: function(data) {
	// 			var args = $.parseJSON(data);
	// 			if (args['status_valid'] == 'false') {
	// 				$(".id-help").remove();
	// 				$('#usernameHelp').after(args['err_username']);
	// 				$('#passwordHelp').after(args['err_password']);
	// 				$('#tipeHelp').after(args['err_type']);
	// 			}else{
	// 				if(args['login'] == 'false'){
	// 					$(".print-error-msg").css('display','block');
	// 				    $(".print-error-msg").html(args['login_info']);
	// 				}else{
	// 					$(".print-error-msg").css('display','none');
	// 				    window.location.href=args['url'] ;
	// 				}
	// 			}
	// 			return false;
	// 		}
	// 	});
    // }); 
    // End Login
});
